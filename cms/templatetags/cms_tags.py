from django import template
from catalog.models import *
from cms.models import Page, News

register = template.Library()

@register.inclusion_tag('main_menu.html', takes_context=True)
def show_menu(context):
    pages = Page.objects.filter(is_active = True).order_by('order')
    return {'pages': pages}

@register.inclusion_tag('bottom_menu.html', takes_context=True)
def show_bottom_menu(context):
    pages = Page.objects.filter(is_active = True).order_by('order')
    return {'pages': pages}

@register.inclusion_tag('cms/news.html')
def show_news():
    news = News.objects.filter(in_archive = False).order_by('-creation_date')[:3]
    return {'news': news}