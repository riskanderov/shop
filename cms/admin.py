from django.contrib.admin.options import ModelAdmin
from models import *
from django.contrib import admin

class NewsAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/js/tiny_mce/tiny_mce.js',
            '/static/js/textareas.js',
            )
class FaqAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/js/tiny_mce/tiny_mce.js',
            '/static/js/textareas.js',
            )

admin.site.register(Page)
admin.site.register(PageTemplate)
admin.site.register(News, NewsAdmin)
admin.site.register(Faq, FaqAdmin)