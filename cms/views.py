from django.http import Http404
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from catalog.models import Product, Article
from cms.models import Page, Faq

def show_page(request, slug):
    page = Page.objects.get(slug = slug)
    if page is None:
        return Http404
    c = RequestContext(request, {'page': page})
    return render_to_response(page.template.template_link, c)

def show_faq(request):
    faqs = Faq.objects.all()
    c = RequestContext(request, {'faq': faqs})
    return render_to_response('cms/faq_list.html', c)

def show_article_list(request):
    articles = Article.objects.all()
    c = RequestContext(request, {'articles': articles})
    return render_to_response('cms/article_list.html', c)

def show_article(request, id):
    article = Article.objects.get(id=id)
    if article is None:
        return Http404
    c = RequestContext(request, {'article': article})
    return render_to_response('cms/article.html', c)

def robots(request):
    c = RequestContext(request, {})
    #response = render_to_response('cms/robots.html', c,  mimetype="text/plain")

    return render_to_response('cms/robots.html', c, mimetype="text/plain")