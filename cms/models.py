# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django.contrib.auth.models import User
from trustedhtml.rules import pretty, normal, full
from trustedhtml.fields import TrustedTextField
from django.utils.html import strip_tags

class PageTemplate(models.Model):
    name = models.CharField(max_length=50)
    template_link = models.CharField(default="cms/page.html", max_length=100)

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = u'Шаблоны страниц'

class Page(models.Model):
    title = models.CharField(max_length=50)
    meta_keywords = models.CharField(max_length=300, blank=True, null=True)
    meta_description = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    order = models.IntegerField(blank=True, null=True)
    template = models.ForeignKey(PageTemplate, blank=True, null=True)
    content = models.TextField(verbose_name=u'Текст', blank=True, null=True)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name_plural = u'Страницы'

class News(models.Model):
    title = models.CharField(max_length=50)
    creation_date = models.DateField(default=datetime.date.today())
    text = models.TextField(max_length=200)
    in_archive = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name_plural = u'Новости'

class Faq(models.Model):
    question = models.TextField(max_length=200, verbose_name=u'Вопрос')
    answer = models.TextField(max_length=1000, verbose_name=u'Ответ')
    def __unicode__(self):
        return strip_tags(self.question)