
from library import MoySklad
from lxml import etree
from catalog.models import *

def UpdateStock():
    moysklad = MoySklad()
    xml = moysklad.GetStockInfo()
    doc = etree.XML(xml.strip())

    items = {}
    for stockTO in doc.iter("stockTO"):
        qty = float(stockTO.get("quantity"))
        price = float(stockTO.get("salePrice"))/100
        productId = stockTO.find("goodRef").get("code")
        items[productId] = {'qty':int(qty), 'price':int(price)}

    products = Product.objects.all()

    site_items = {}
    for prod in products:
        site_items[prod.id] = prod

    for k in site_items.keys():
        need_update_card = False
        if items.has_key(str(k)) and items[str(k)]['qty'] > 0:
            site_items[k].qty_avail = items[str(k)]['qty']
            site_items[k].base_price = items[str(k)]['price']
            need_update_card = True
        else:
            site_items[k].qty_avail = 0
        site_items[k].save()
        if site_items[k].showcase is not None:
            card = ProductCard.objects.get(id=site_items[k].showcase.id)
            if card is not None and need_update_card:
                card.base_price = site_items[k].base_price
                card.save()
                #card_products = Product.objects.filter(showcase__id=card[0].id)
                #is_card_active = False
                #for product in card_products:
                #    if product.qty_avail > 0 or (product.fivelb_available == True and product.fivelb_sync_active==True):
                #        if not is_card_active:
                #            is_card_active = True

def SyncCategories():
    moysklad = MoySklad()
    xml = moysklad.DoGetRequest('GoodFolder/list')
    doc = etree.XML(xml.strip())

    ms_categories = {}
    for goodFolder in doc.iter("goodFolder"):
        name = goodFolder.get("name")
        codetag = goodFolder.find("code")
        if codetag is None:
            continue
        code = int(codetag.text)
        ms_categories[code]=name

    pf_categories = {}
    cat = Category.objects.filter(is_active=True)
    for pf_category in cat:
        pf_categories[pf_category.id]=pf_category.name

    for k in pf_categories:
        if not ms_categories.has_key(k):
            AddMsCategory(k, pf_categories[k])

def SyncProducts():
    moysklad = MoySklad()
    xml = moysklad.DoGetRequest('Good/list')
    doc = etree.XML(xml.strip())

    ms_products = {}
    for good in doc.iter("good"):
        name = good.get("name")
        codetag = good.find("code")
        if codetag is None or codetag.text is None:
            continue
        code = int(codetag.text)
        ms_products[code]=name

    products = Product.objects.all()

    pf_products = {}
    for prod in products:
        pf_products[prod.id] = prod

    for k in pf_products.keys():
        if not ms_products.has_key(k):
            name = pf_products[k].name
            if pf_products[k].taste:
                name += ' '+pf_products[k].taste.value
            if pf_products[k].uom > 0:
                name += ' '+str(pf_products[k].uom_units) +' '+pf_products[k].get_uom_display()
            AddMsProduct(k, name)

def AddMsCategory(id, name):
    moysklad = MoySklad()
    root = etree.Element("goodFolder", name=name)
    code = etree.SubElement(root, "code")
    code.text = str(id)

    xml = etree.tostring(root, encoding='utf-8', xml_declaration=True)

    moysklad.DoPutRequest('GoodFolder', xml)

def AddMsProduct(id, name):
    moysklad = MoySklad()
    root = etree.Element("good", name=name)
    code = etree.SubElement(root, "code")
    code.text = str(id)

    xml = etree.tostring(root, encoding='utf-8', xml_declaration=True)

    moysklad.DoPutRequest('Good', xml)
