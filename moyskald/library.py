import urllib2
import urllib

class MoySklad:
    __host = 'online.moysklad.ru/exchange/rest/ms/xml'
    __stockhost = 'online.moysklad.ru/exchange/rest'

    def __init__(self, login='admin@pitfit', password='bc0a9ff0e2'):
        self.login = login
        self.password = password

    def __sendGetRequest(self, uri, params=None):
        url = self.__getUrl(uri, params)
        print url
        request = urllib2.Request(url)
        passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, url, self.login, self.password)
        authhandler = urllib2.HTTPBasicAuthHandler(passman)
        try:
            opener = urllib2.build_opener(authhandler)
            data = opener.open(request).read()
            return data
        except IOError, e:
            return e.code

    def __sendGetStockRequest(self, uri, params=None):
        url = self.__getStockUrl(uri, params)
        print url
        request = urllib2.Request(url)
        passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, url, self.login, self.password)
        authhandler = urllib2.HTTPBasicAuthHandler(passman)
        try:
            opener = urllib2.build_opener(authhandler)
            data = opener.open(request).read()
            return data
        except IOError, e:
            return e.code

    def __sendPutRequest(self, uri, body, params=None):
        url = self.__getUrl(uri)
        request = urllib2.Request(url, body)
        request.get_method = lambda: 'PUT'
        request.add_header('Content-Type','application/xml')
        passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, url, self.login, self.password)
        authhandler = urllib2.HTTPBasicAuthHandler(passman)
        try:
            opener = urllib2.build_opener(authhandler)
            data = opener.open(request).read()
            return data
        except IOError, e:
            return e.code

    def __getUrl(self, uri, params=None):
        url = "https://%s/%s" % (self.getHost(), uri)
        paramStr = ''
        if params is not None:
            for k, v in params.items():
                if v is None:
                    del params[k]
            paramStr = urllib.urlencode(params)
        return "%s?%s" % (url, paramStr)

    def __getStockUrl(self, uri, params=None):
        url = "https://%s/%s" % (self.getStockHost(), uri)
        paramStr = ''
        if params is not None:
            for k, v in params.items():
                if v is None:
                    del params[k]
            paramStr = urllib.urlencode(params)
        return "%s?%s" % (url, paramStr)

    def getHost(self):
        return self.__host

    def getStockHost(self):
        return self.__stockhost


    def DoGetRequest(self, uri, params=None):
        return self.__sendGetRequest(uri, params)

    def DoPutRequest(self, uri, body, params=None):
        return self.__sendPutRequest(uri, body, params)

    def GetStockInfo(self):
        return self.__sendGetStockRequest('stock/xml')