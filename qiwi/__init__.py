from django.conf import settings
from django_qiwi.soap.client import Client as QiwiClient
from order.models import *
import logging


def update_bill(txn, status):
    client = QiwiClient()
    response = client.checkBill(txn)
    code = response['status']
    logging.basicConfig(filename=settings.QIWI_LOG_FILE, level=logging.DEBUG)
    logging.debug(str(txn))
    orders = Order.objects.filter(order_guid=txn)
    if orders.exists():
        order = orders[0]
        if code == 60:
            order.order_status = 'C'
            order.save()
            ret = 0
        elif code in [150, 160, 161]:
            order.order_status = 'D'
            order.save()
            ret = 0
        else:
            ret = -1
    else:
        ret = 210
    logging.basicConfig(filename=settings.QIWI_LOG_FILE, level=logging.DEBUG)
    return ret

