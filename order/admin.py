from django.contrib.admin.options import ModelAdmin
from django.contrib.contenttypes import generic
from django.forms.models import BaseInlineFormSet
from models import *
from django.contrib import admin

class OrderDetailFormSet(BaseInlineFormSet):
    def get_queryset(self):
        if not hasattr(self, '_queryset'):
            qs = super(OrderDetailFormSet, self).get_queryset().filter(product__category__slug='bcaa')
            self._queryset = qs
        return self._queryset

class OrderDetailInline(admin.TabularInline):
    model = OrderDetail
    extra = 0
    fields = ('product', 'qty', 'price')

class OrderAdmin(ModelAdmin):
    inlines = [
        OrderDetailInline,
    ]
    list_display = ('order_client', 'contact_phone', 'order_price', 'shipment_type', 'order_status', 'date_added')
    list_filter = ['date_added', 'order_status']

admin.site.register(Order, OrderAdmin)
admin.site.register(ShipmentType)