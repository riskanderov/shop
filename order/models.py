# -*- coding: utf-8 -*-
from django.db.models.aggregates import Count
from django.db.models.signals import post_save
from django.core.mail import send_mail, EmailMultiAlternatives
from django.db import models
from django.template import Context
from django.template.loader import get_template
from catalog.models import *
from datetime import datetime, date, time, timedelta
import re
import uuid
from django_qiwi.soap.client import Client as QiwiClient
from django_qiwi import get_status_text

class ShipmentType(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField(default=0)
    type_id = models.CharField(max_length=10)
    default = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = u'Типы доставки'

class Order(models.Model):
    ORDER_STATUS = (
        ('N', u'Новый'),
        ('P', u'В обработке'),
        ('C', u'Выполнен'),
        ('D', u'Отменен'),
        ('S', u'В доставке'),
        )
    PAYMENT_TYPE = (
        ('QW', u'QIWI'),
        ('RC', u'Квитанция'),
        #    ('WM', u'WebMoney'),
        #    ('YA', u'Яндекс Деньги'),
        ('CS', u'Наличными')
        )
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(blank=True, null=True)
    customer = models.ForeignKey(User, blank=True, null=True)
    anonym = models.ForeignKey(Client, blank=True, null=True)
    contact_phone = models.CharField(max_length=50)
    shipment_address = models.TextField(blank=True, null=True)
    order_comment = models.TextField(blank=True, null=True)
    notify_customer = models.BooleanField(default=True)
    order_status = models.CharField(max_length=1, choices=ORDER_STATUS, default='N')
    shipment_type = models.ForeignKey(ShipmentType)
    shipment_price = models.IntegerField(default=0)
    order_price = models.IntegerField(default=0)
    payment_method = models.CharField(max_length=2, choices=PAYMENT_TYPE, default='CS')
    order_guid = models.CharField(max_length=32, blank=True)

    def __unicode__(self):
        return unicode(self.date_added)

    def order_client(self):
        if self.customer is None:
            return self.anonym
        else:
            return '%s (%s)' % (self.customer.first_name, self.customer.username)

    def save(self, *args, **kwargs):
        if not self.id:
            self.date_added = datetime.today()
        if not self.order_guid or self.order_guid == u'':
            self.order_guid = uuid.uuid4().get_hex()
        super(Order, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = u'Заказы'

class  OrderDetail(models.Model):
    product = models.ForeignKey(Product)
    order = models.ForeignKey(Order, related_name='details')
    qty = models.IntegerField(default=1)
    price = models.IntegerField(default=0)

    def __unicode__(self):
        return '%s [%s] %s %s' % (self.product.name, self.product.taste or u'Без вкуса', self.product.uom_units, self.product.uom_display)

    @property
    def fivelb_product(self):
        fivelb_product = Product.objects.filter(id = self.product.id, fivelb_available=True, qty_avail=0)
        return fivelb_product.count() > 0

def send_order_notification(sender, **kwargs):
    order = kwargs['instance']
    contains_details = OrderDetail.objects.filter(order__id = order.id).annotate(Count('id'))
    if contains_details.count() > 0 and order.date_modified is None:

        plain_admin_tmpl = get_template('order/admin_notification.txt')
        plain_user_tmpl = get_template('order/user_notification.txt')
        html_admin_tmpl = get_template('order/admin_notification.html')
        html_user_tmpl = get_template('order/user_notification.html')
        client = order.customer
        if client is None:
            client = order.anonym
            email = client.email
            name = client.name
        else:
            email = client.email
            name = "%s %s" % (client.first_name, client.last_name)
        d = Context({'order': order, 'details': contains_details, 'name': name})
        text_admin_content = plain_admin_tmpl.render(d)
        text_user_content = plain_user_tmpl.render(d)
        msg = EmailMultiAlternatives(u'Создан новый заказ №%s' % order.id, text_admin_content, 'mail@pitfit.ru', ['sm@pitfit.ru', 'roman.iskanderov@gmail.com'])
        msg.send()
        order.date_modified = datetime.today()
        order.save()
        umsg = EmailMultiAlternatives(u'Магазин спортивного питания PitFit.ru. Заказ №%s' % order.id, text_user_content, 'mail@pitfit.ru', [email])
        umsg.send()

def qiwi_invoice(order):
    client = QiwiClient()
    phone = re.sub(r'[\D]','', order.contact_phone)

    if len(phone) == 11 and phone[0] in ['7','8']:
        phone = phone[1:]
        send_mail(u'Пытаемся выставить счет QIWI для заказа №%s' % order.id,
            u'Формат телефона: %s преобразован в %s \n'\
            u'Сумма заказа: %s' % (order.contact_phone, phone, order.order_price),
            'mail@pitfit.ru', ['roman.iskanderov@gmail.com'])
    elif len(phone) == 10 and phone[0] == '9':
        pass
    else:
        send_mail(u'Неудалось выставить счет QIWI для заказа №%s' % order.id,
            u'Телефон: %s преобразован в %s\n'\
            u'Сумма заказа: %s' % (order.contact_phone, phone, order.order_price),
            'mail@pitfit.ru', ['roman.iskanderov@gmail.com'])
        return 0

    code = client.createBill(
        phone = phone,
        amount = order.order_price+order.shipment_price,
        comment = u'Счет на спортивное питание PITFIT.RU',
        txn = order.order_guid,
        lifetime = datetime.now() + timedelta(7)
    )
    if code:
        send_mail(u'Неудалось выставить счет QIWI для заказа №%s' % order.id,
            u'Телефон: %s преобразован в %s\n'\
            u'Сумма заказа: %s' % (order.contact_phone, phone, order.order_price),
            'mail@pitfit.ru', ['roman.iskanderov@gmail.com'])
        return 0

def do_invoice(sender, **kwargs):
    order = kwargs['instance']
    if order.payment_method == 'QW':
        qiwi_invoice(order)

post_save.connect(send_order_notification, sender=Order)
post_save.connect(do_invoice, sender=Order)