# -*- coding: utf-8 -*-
import re
from django import forms
from registration.forms import RegistrationFormTermsOfService, RegistrationFormUniqueEmail, attrs_dict
from django.contrib.auth.models import User

class OrderForm(forms.Form):
    phone = forms.CharField(label=u'Телефон', required=True)
    address = forms.CharField(label=u'Адрес доставки', required=True)
    comment = forms.CharField(label=u'Комментарий', required=False)
    shipment_type = forms.CharField(label=u'Доставка')

    def clean(self):
        cleaned_data = self.cleaned_data
        phone = cleaned_data.get("phone", None)

        if phone is not None:
            p = re.compile(r'\D')
            phone = p.sub('', phone)
            p = re.compile(r'\d')

            if p.match(phone) is None:
                msg = u'Телефон должен содержать цифры'
                self._errors['phone'] = self.error_class([msg])
                del cleaned_data['phone']
        else:
            self._errors['phone'] = self.error_class([u"Необходимо указать телефон"])

        return cleaned_data

class RegOrderForm(OrderForm):
    email = forms.EmailField(label=u'E-mail')
    name = forms.CharField(label=u'Имя Фамилия')
    password1 = forms.CharField(label=u'Пароль')
    password2 = forms.CharField(label=u'Пароль еще раз')
    shipment_type = forms.CharField(label=u'Доставка')

    def clean(self):
        cleaned_data = self.cleaned_data
        email = cleaned_data.get("email")
        name = cleaned_data.get("name")
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")
        phone = cleaned_data.get("phone", None)
        try:
            user = User.objects.get(email=email)
            msg = u'Email занят'
            self._errors['email'] = self.error_class([msg])
            del cleaned_data['email']
        except User.DoesNotExist:
            pass

        if password1 != password2:
            msg = u'Пароль не совпадает'
            self._errors['password2'] = self.error_class([msg])
            if cleaned_data.get('password2', None):
                del cleaned_data['password2']
            if cleaned_data.get('password1', None):
                del cleaned_data['password1']

        if phone is not None:
            p = re.compile(r'\D')
            phone = p.sub('', phone)
            p = re.compile(r'\d')

            if p.match(phone) is None:
                msg = u'Телефон должен содержать цифры'
                self._errors['phone'] = self.error_class([msg])
                del cleaned_data['phone']
        else:
            self._errors['phone'] = self.error_class([u"Необходимо указать телефон"])

        return cleaned_data

class FastOrderForm(OrderForm):
    name = forms.CharField(label=u'Имя Фамилия', required=True)
    phone = forms.CharField(label=u'Телефон', required=True)
    email = forms.EmailField(label='E-mail', required=True)
    shipment_type = forms.CharField(label=u'Доставка')