
from django.conf import settings
from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from dajaxice.core import dajaxice_autodiscover
from django.conf.urls.defaults import *

admin.autodiscover()
dajaxice_autodiscover()

urlpatterns = patterns('',
    # Examples:
     url(r'^$', 'catalog.views.index', name='index'),
     url(r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
     url(r'^brand/$', 'catalog.views.brand_filter'),
     url(r'^brand/(?P<brand>[A-Za-z0-9_-]+)/$', 'catalog.views.brand_filter'),
     url(r'^brand/(?P<brand>[A-Za-z0-9_-]+)/(?P<category>[A-Za-z0-9_-]+)/$', 'catalog.views.brand_filter'),
     url(r'^category/$', 'catalog.views.category_filter'),
     url(r'^category/(?P<category>[A-Za-z0-9_-]+)/$', 'catalog.views.category_filter'),
     url(r'^category/(?P<category>[A-Za-z0-9_-]+)/(?P<brand>[A-Za-z0-9_-]+)/$', 'catalog.views.category_filter'),
     url(r'^cart/$','cart.views.cart'),
     url(r'^cart/checkout/$','cart.views.check_out'),
     url(r'^cart/create/order/$','cart.views.create_order'),
     url(r'^cart/create/fastorder/$','cart.views.create_fast_order'),
     url(r'^cart/create/regorder/$','cart.views.create_reg_order'),
     url(r'^catalog/product/(?P<product_id>\w+)/$','catalog.views.product_main'),
     url(r'^catalog/pricelist/(?P<discount>\d{2})/$','catalog.views.catalog_price'),
     url(r'^catalog/fullpricelist/$','catalog.views.full_product_list'),
     url(r'^catalog/outofstock/$','catalog.views.products_out_of_stock'),
     url(r'^tag/(?P<tag>[^/]+)/$','catalog.views.show_objects_by_tag', name='tag'),
     url(r'^mission/(?P<mission>[A-Za-z0-9_-]+)/$','catalog.views.show_mission_complexes',),
     url(r'^account/login/$', 'customers.views.login', name='login'),
     url(r'^account/logout/$', 'customers.views.logout', name='logout'),
     url(r'^account/register/$', 'customers.views.register', name='register'),
     url(r'^faq/$', 'cms.views.show_faq'),
     url(r'^article/$', 'cms.views.show_article_list'),
     url(r'^article/(?P<id>\w+)/$', 'cms.views.show_article'),
     #url(r'^account/reminder/$', password_reset, name='reset'),
     url(r'^search/', include('haystack.urls')),
     url(r'^yml.xml$', 'catalog.views.yml'),
     url(r'^tagging_autocomplete/', include('tagging_autocomplete.urls')),
#     url(r'^moysklad-yml.xml$', 'catalog.views.moysklad_yml'),
#     url(r'^moysklad-receipt-csv$', 'catalog.views.moysklad_receipt_csv'),
#     url(r'^accounts/register/$', register, {'backend': 'customers.forms.PFRegistrationBackend',
#                                                 'form_class': CustomRegForm},
#                                  name='registration_register'),
#
#     url(r'^accounts/', include('registration.backends.default.urls')),
     url(r'^admin/', include(admin.site.urls)),
     url(r'^robots.txt$', 'cms.views.robots'),
     url(r'^(?P<slug>[A-Za-z0-9-]+)/$', 'cms.views.show_page'),
    # url(r'^shop/', include('foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:

)
