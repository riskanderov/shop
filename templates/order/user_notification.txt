Спасибо за заказ!

Номер вашего заказа: {{order.id}}

Вы заказали:
{% for detail in details %}
    {{forloop.counter}}. {{detail.product.name|upper}} {{detail.product.taste}} {{detail.product.uom_units}} {{detail.product.uom_display}} [ Цена: {{detail.price}} руб; Количество в заказе: {{detail.qty}} ]
{% endfor %}

Итоговая стоимость заказа: {{ order.order_price }}

Адрес доставки: {{ order.shipment_address }}
Контактный номер: {{order.contact_phone}} ({{name}})

Интернет-магазин спортивного питания PITFIT.RU


