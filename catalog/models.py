# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from trustedhtml.rules import pretty, normal, full
from south.modelsinspector import add_introspection_rules
from trustedhtml.fields import TrustedTextField
from mptt.models import MPTTModel, TreeForeignKey
from tagging.fields import TagField
from sorl.thumbnail import get_thumbnail
from tagging_autocomplete.models import TagAutocompleteField

class Category(MPTTModel):
    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    meta_title = models.CharField(max_length=100, blank=True, null=True)
    meta_description = models.CharField(max_length=300, blank=True, null=True)
    meta_keywords = models.CharField(max_length=300, blank=True, null=True)
    description = models.TextField(verbose_name=u'Описание категории')
    title_for_product = models.CharField(max_length=100, blank=True, null=True, verbose_name=u'Заголовок на странице товара')
    is_active = models.BooleanField(default=True)
    available_in_menu = models.BooleanField(default=True, verbose_name=u'Показывать в меню')
    parent = TreeForeignKey('self', blank=True, null=True, related_name='child', verbose_name=u'Родительская категория')
    menu_order = models.IntegerField(default=0,unique=True)

    def __unicode__(self):
        return self.name
    class MPTTMeta:
        order_insertion_by = ['name']
    class Meta:
        verbose_name_plural = u'Категории'

class Brand(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    meta_title = models.CharField(max_length=100, blank=True, null=True)
    meta_description = models.CharField(max_length=300, blank=True, null=True)
    meta_keywords = models.CharField(max_length=300, blank=True, null=True)
    descripiton = models.TextField(verbose_name=u'Описание бренда')
    is_active = models.BooleanField(default=True)
    image = models.ImageField(upload_to='brands', verbose_name=u'Логотип производителя 180х180', blank=True, null=True)
    fivelb_name = models.CharField(blank=True, null=True, max_length=100, verbose_name=u'Имя бренда в 5lb')
    fivelb_search_page = models.CharField(blank=True, null=True, max_length=100, verbose_name=u'Страница бренда в 5lb')
    fivelb_sync_available = models.BooleanField(default=False, verbose_name=u'Синхронизировать с 5LB')

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = u'Производитель'

class CustomerGroup(models.Model):
    name = models.CharField(max_length=100)
    default_discount = models.DecimalField(default=0,decimal_places=2,max_digits=12)

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = u'Группы клиентов'

class ProductPrice(models.Model):
    price = models.DecimalField(decimal_places=2,max_digits=12)
    customer_group = models.ForeignKey(CustomerGroup)

class Promo(models.Model):
    name = models.CharField(max_length=40, verbose_name=u'Название акции')
    discount = models.IntegerField(default=0, verbose_name=u'Скидка %')

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.discount)

    class Meta:
        verbose_name_plural = u'Спец акции'


class ProductCardManager(models.Manager):

    def available_variants(self):
        qa = self.extra(select={'avail_count':
        '''SELECT COUNT(*) FROM catalog_product
           WHERE catalog_product.showcase_id=catalog_productcard.id
           AND (catalog_product.qty_avail > 0 OR catalog_product.fivelb_available = True)'''}).extra(where = ['avail_count > 0'])

        return qa


class ProductCard(models.Model):
    UOMPS = (
        ('KG', u'кг'),
        ('GR', u'гр'),
        #('UZ', u''),
        ('LT', u'л'),
        ('ML', u'мл'),
        ('TB', u'таб'),
        ('CP', u'каплет'),
        ('CS', u'капс'),
        ('BT', u'бат'),
        ('PK', u'пак'),
        ('AM', u'амп')
    )
    name = models.CharField(max_length=100, verbose_name=u'Наименование')
    slug = models.SlugField(unique=True, blank=True, null=True, verbose_name=u'Имя для ссылки URL')
    meta_title = models.CharField(max_length=100, blank=True, null=True)
    meta_description = models.CharField(max_length=300, blank=True, null=True)
    meta_keywords = models.CharField(max_length=300, blank=True, null=True)
    uom = models.CharField(max_length=10, choices=UOMPS, default='KG', verbose_name=u'Единица измерения')
    uom_units = models.DecimalField(decimal_places=2, max_digits=10, default=0, verbose_name=u'Количество в упаковке')
    base_price = models.IntegerField(default=0, verbose_name=u'Цена')
    old_price = models.IntegerField(default=0, verbose_name=u'Старая цена',blank=True, null=True)
    is_active = models.BooleanField(default=True, verbose_name=u'Активен')
    is_hit = models.BooleanField(default=False, verbose_name=u'Хит')
    is_recomended = models.BooleanField(default=False, verbose_name=u'Рекомендуем')
    description = TrustedTextField(validator=pretty, verbose_name=u'Описание товара')
    consist = TrustedTextField(validator=pretty, verbose_name=u'Состав', blank=True, null=True)
    take_recommend = TrustedTextField(validator=pretty, verbose_name=u'Способ применения', blank=True, null=True)
    related_products = models.ManyToManyField('self' , null=True, blank=True, verbose_name=u'Продукты в догонку')
    category = models.ManyToManyField(Category, verbose_name=u'Категория')
    image = models.ImageField(upload_to='products', verbose_name=u'Фото товара', blank=True, null=True)
    brand = models.ForeignKey(Brand, verbose_name=u'Производитель')
    modified = models.DateTimeField(null=True, blank=True, auto_now=True)
    promo = models.ForeignKey(Promo, blank=True, null=True, verbose_name=u'Спец. акция' )

    def __unicode__(self):
        if self.promo:
            return "%s %s %s" % (self.name, self.uom_units, self.promo)
        return "%s %s" % (self.name, self.uom_units)

    def uniq_category(self):
        category = Category.objects.filter(productcard__id=self.id)[:1]
        return category[0]

    def variants(self):
        #variants = self.product_set.all().filter(Q(qty_avail__gt=0)|Q(fivelb_avail=True))
        variants = Product.objects.filter(showcase__id = self.id).filter(Q(qty_avail__gt=0) | Q(fivelb_available=True))
        return variants

    @property
    def is_available(self):
        variants = Product.objects.filter(showcase__id = self.id).filter(Q(qty_avail__gt=0) | Q(fivelb_available=True))
        return variants.count() > 0


    @property
    def uom_display(self):
        return self.get_uom_display()

    @property
    def customer_price(self):
        if self.promo:
            return int(self.base_price - int(self.base_price*(int(self.promo.discount)/100.0)))
        return self.base_price

    def save(self, *args, **kwargs):
        self.description = pretty.validate(self.description)

        self.modified = datetime.datetime.now()
        super(ProductCard, self).save(*args, **kwargs)

    def market_thumbnail(self):
        im = get_thumbnail(self.image, '300x300')
        return im

    class Meta:
        verbose_name_plural = u'Товарные Карточки'
        ordering = ['name']

class Product(models.Model):
    UOM = (
        ('KG', u'кг'),
        ('GR', u'гр'),
        #('UZ', u''),
        ('LT', u'л'),
        ('ML', u'мл'),
        ('TB', u'таб'),
        ('CP', u'каплет'),
        ('CS', u'капс'),
        ('BT', u'бат'),
        ('PK', u'пак'),
        ('AM', u'амп')
    )
    name = models.CharField(max_length=100, verbose_name=u'Наименование')
    slug = models.SlugField(unique=True, blank=True, null=True, verbose_name=u'Имя для ссылки URL')
    meta_title = models.CharField(max_length=100, blank=True, null=True)
    meta_description = models.CharField(max_length=300, blank=True, null=True)
    meta_keywords = models.CharField(max_length=300, blank=True, null=True)
    sku = models.CharField(max_length=50, blank=True, null=True)
    uom = models.CharField(max_length=10, choices=UOM, default='KG', verbose_name=u'Единица измерения')
    uom_units = models.DecimalField(decimal_places=2, max_digits=10, default=0, verbose_name=u'Количество в упаковке')
    base_price = models.IntegerField(default=0, verbose_name=u'Цена')
    old_price = models.IntegerField(default=0, verbose_name=u'Старая цена',blank=True, null=True)
    is_active = models.BooleanField(default=True, verbose_name=u'Активен')
    is_hit = models.BooleanField(default=False, verbose_name=u'Хит')
    is_recomended = models.BooleanField(default=False, verbose_name=u'Рекомендуем')
    description = models.TextField(verbose_name=u'Описание товара')
    consist = models.TextField(verbose_name=u'Состав', blank=True, null=True)
    take_recommend = models.TextField(verbose_name=u'Способ применения', blank=True, null=True)
    related_products = models.ManyToManyField('self', null=True, blank=True, verbose_name=u'Продукты в догонку')
    category = models.ManyToManyField(Category, verbose_name=u'Категория')
    image = models.ImageField(upload_to='products', verbose_name=u'Фото товара', blank=True, null=True)
    brand = models.ForeignKey(Brand, verbose_name=u'Производитель')
    tag = TagAutocompleteField(blank=True, null=True)
    qty_avail = models.IntegerField(default=0, verbose_name=u'На складе', blank=True, null=True)
    modified = models.DateTimeField(null=True, blank=True)

    showcase = models.ForeignKey(ProductCard, blank=True, null=True, verbose_name=u'Название товара на витрине', related_name='products')
    fivelb_sko = models.IntegerField(blank=True, null=True)
    fivelb_available = models.BooleanField(default=False, verbose_name=u'Доступен в 5lb')
    fivelb_sync_active = models.BooleanField(default=False, verbose_name=u'Синхронизировать с 5lb')

    def __unicode__(self):
        return "%s %s %s" % (self.name, self.taste, self.uom_units)

    def uniq_category(self):
        category = Category.objects.filter(product__id=self.id)[:1]
        return category[0]

    @property
    def taste(self):
        option = ProductOption.objects.filter(product__id=self.id)
        if len(option) < 1:
            return None
        return option[0].value

    @property
    def uom_display(self):
        return self.get_uom_display()

    @property
    def uom_lb_display(self):
        if self.uom == 'KG':
            if self.uom_units > 0.8 and self.uom_units < 1.1:
                return '2Lb'
            if self.uom_units > 2 and self.uom_units < 2.4:
                return '5Lb'
        if self.uom == 'GR':
            if self.uom_units > 800 and self.uom_units < 1100:
                return '2Lb'
            if self.uom_units > 2000 and self.uom_units < 2400:
                return '5Lb'
        return ''

    def save(self, *args, **kwargs):
        self.description = pretty.validate(self.description)

        self.modified = datetime.datetime.now()
        super(Product, self).save(*args, **kwargs)

    def market_thumbnail(self):
        im = get_thumbnail(self.image, '300x300')
        return im

    class Meta:
        verbose_name_plural = u'Товары'

class Client(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Имя', default=u'Анонимный клиент')
    phone = models.CharField(max_length=20, verbose_name=u'Телефон')
    address = models.TextField(verbose_name=u'Адрес доставки')
    email = models.EmailField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True, verbose_name=u'Комментарий клиента')

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.email)
    class Meta:
        verbose_name_plural = u'Анонимы'

class Option(models.Model):
    name = models.CharField(max_length=50, verbose_name=u'Название')
    description = models.TextField(verbose_name=u'Описание', blank=True, null=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = u'Свойства товаров'

class OptionValue(models.Model):
    option = models.ForeignKey(Option)
    product = models.ManyToManyField(Product, blank=True, null=True, related_name='option_values', through='ProductOption')
    value = models.CharField(max_length=100)
    def __unicode__(self):
        return self.value
    class Meta:
        verbose_name_plural = u'Значения свойств товаров'

class ProductOption(models.Model):
    product = models.ForeignKey(Product)
    option = models.ForeignKey(Option)
    value = models.ForeignKey(OptionValue)

class Article(models.Model):
    title = models.CharField(max_length=200)
    meta_description = models.CharField(max_length=300, blank=True, null=True)
    meta_keywords = models.CharField(max_length=300, blank=True, null=True)
    content = models.TextField( verbose_name=u'Текст статьи')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural = u'Статьи'

class ProductComplex(models.Model):
    name = models.CharField(max_length=100)
    product = models.ManyToManyField(Product, related_name='pcomplex', verbose_name=u'Товары в комплексе')

    def __unicode__(self):
        return self.name

    @property
    def is_active(self):
        products = Product.objects.filter(pcomplex__id = self.id)
        for complex_item in products:
            if complex_item.qty_avail == 0:
                return False
        return True

    @property
    def complex_price(self):
        price = 0
        products = Product.objects.filter(pcomplex__id = self.id)
        for complex_item in products:
            price += complex_item.base_price
        return price

    class Meta:
        verbose_name_plural = u'Комплексы продуктов'

class Mission(models.Model):
    name = models.CharField(max_length=40)
    slug = models.SlugField(unique=True)
    is_active = models.BooleanField(default=True)
    weight = models.IntegerField(default=0, blank=True, null=True)
    article = models.ForeignKey(Article, blank=True, null=True)
    complex = models.ManyToManyField(ProductComplex, related_name='mission', verbose_name=u'Комплексы продуктов для достижения')
    meta_title = models.CharField(max_length=100, blank=True, null=True)
    meta_description = models.CharField(max_length=300, blank=True, null=True)
    meta_keywords = models.CharField(max_length=300, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = u'Цели'

class Raiting(models.Model):
    vote = models.IntegerField(default=0, verbose_name=u'Оценка')
    product = models.ForeignKey(Product)
    def __unicode__(self):
        return '%s (%s)' % (self.product.name, self.vote)


class Appreciation(models.Model):
    author_name = models.CharField(max_length=100)
    rating = models.IntegerField(default=0)
    product = models.ForeignKey(Product)
    comment = models.CharField(max_length=400)
    date = models.DateField(auto_now_add=True)
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s (%s)' % (self.product.name, self.date)

    class Meta:
        verbose_name_plural = u'Отзывы по товарам'

class FiveLB_Product(models.Model):
    name = models.CharField(max_length=100)
    sko = models.IntegerField()
    brand = models.CharField(max_length=100, blank=True, null=True)
    price = models.IntegerField()
    available = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s %s' % (self.name, self.brand)
