from django import template
from django.db.models.aggregates import Count
from catalog.models import *
from catalog.object import SeenScope
from tagging.models import Tag, TaggedItem

register = template.Library()


@register.inclusion_tag('brands.html', takes_context=True)
def show_brands(context):
    brands = Brand.objects.filter(is_active=True).order_by('name')
    filter = ''
    category = ''
    current = ''
    if 'filter' in context:
        filter = context['filter']
    if 'brand' in context:
        current = context['brand']
    return {'brands': brands, 'brand_filter': filter, 'category': category, 'current_brand': current}

@register.inclusion_tag('categories.html', takes_context=True)
def show_categories(context):
    categories = Category.objects.filter(is_active=1, available_in_menu=1).order_by('menu_order')
    filter = ''
    brand = ''
    current = ''
    if 'filter' in context:
        filter = context['filter']
    if 'category' in context:
        current = context['category']
    return {'categories': categories, 'category_filter': filter, 'brand': brand, 'current_category': current}

@register.filter
def truncchar(value, arg):
    if len(value) < arg:
        return value
    else:
        return value[:arg] + '...'

@register.filter
def update(value, arg):
    result = value * ( 1 - arg/100.00 )
    return int(result)

@register.inclusion_tag('dropdown_filter.html', takes_context=True)
def show_global_filter(context):
    categories = Category.objects.filter(is_active=1, available_in_menu=1).order_by('name')
    brands = Brand.objects.filter(is_active=True).order_by('name')
    current_brand = ''
    current_category = ''
    if 'brand' in context:
        current_brand = context['brand']
    if 'category' in context:
        current_category = context['category'].lower()

    return {'categories': categories,'brands': brands,'current_brand': current_brand,'current_category':current_category }

@register.inclusion_tag('catalog/subfilter.html', takes_context=True)
def show_sub_filter(context):
    current_brand = ''
    current_category = ''

    if 'brand' in context:
        current_brand = context['brand']
    if 'category' in context:
        current_category = context['category'].lower()

    filter_type = 'available'
    if current_brand != '' and (current_category == '' or current_category == 'none'):
        filter_type = 'brand_categories'
    elif current_category != '' and (current_brand == '' or current_brand == 'none'):
        filter_type = 'category_brands'

    if filter_type == 'brand_categories':
        categories = Category.objects.filter(is_active=True, available_in_menu=True).filter(productcard__brand__slug=current_brand, productcard__brand__is_active=True).annotate(qty=Count('productcard'))
        fb = Brand.objects.get(slug=current_brand)
        return {'categories':categories, 'brand': current_brand, 'filtered_brand':fb}
    if filter_type == 'category_brands':
        brands = Brand.objects.filter(is_active=True).filter(productcard__category__slug=current_category, productcard__category__is_active=True).annotate(qty=Count('productcard'))
        fc = Category.objects.get(slug=current_category)
        return {'brands':brands, 'category': current_category, 'filtered_category':fc}


    return {'avalable': True }

@register.inclusion_tag('breadcrumbs.html', takes_context=True)
def show_breadcrumbs(context):
    brand = ''
    category = ''
    if 'brand' in context:
        if context['brand'] != '' and context['brand'] != 'none':
            brand = Brand.objects.get(slug=context['brand'])
    if 'category' in context:
        if context['category'] != '' and context['category'] != 'none':
            category = Category.objects.get(slug=context['category'].lower())
    return {'brand': brand, 'category': category}

@register.inclusion_tag('seen_scope.html', takes_context=True)
def show_seen_scope(context):
    request = context['request']
    seenScope = request.session.get('seen_scope', None) or SeenScope

    available = False
    if SeenScope.itemsCount.__get__(seenScope, SeenScope) > 0:
        available = SeenScope.itemsCount.__get__(seenScope, SeenScope)
    return {'already_seen': seenScope, 'available': available}

@register.inclusion_tag('missions.html')
def show_missions():
    missions = Mission.objects.filter(is_active=True)

    return {'missions': missions}

@register.inclusion_tag('popular_tags.html')
def show_popular_tags():
    tags = Tag.objects.annotate(num_items=Count('items')).order_by('-num_items')[:5]

    return {'tags': tags}

