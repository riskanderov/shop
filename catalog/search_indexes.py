# -*- coding: utf-8 -*-
from haystack.indexes import *
from haystack import site
from models import ProductCard

class ProductCardIndex(RealTimeSearchIndex ):
    name = CharField(document=True, use_template=True)
    description = CharField(model_attr='description')
    def index_queryset(self):
        return ProductCard.objects.filter(is_active=True)

site.register(ProductCard, ProductCardIndex)
