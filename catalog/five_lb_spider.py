from grab.spider import Spider, Task
from models import *
from grab import *
from lxml import html

additional_headers = {
     'Accept-Charset': 'utf-8',
     'User-Agent': 'Googlebot/2.1 (+http://www.google.com/bot.html)'
}

class FiveLbSpider(Spider):
    def __init__(self, target_url='bsn.html'):
        self.target_url = 'http://www.5lb.ru/producers/'+target_url
        Spider.__init__(self)
        self.setup_grab(headers=additional_headers)
    #initial_urls = ['http://www.5lb.ru/producers/bsn.html']
    def parse_products(self, grab):
        g = Grab()
        g.setup(headers={'X-Requested-With':'XMLHttpRequest'})
        g.go(self.target_url)
        producer_id = g.doc.select('//div[@class="product-list"]').attr('data-args').replace("=", "%3D")
        print producer_id
        first_element = g.doc.select('//div[@class="product-list"]/ul/li').html()
        g.go("http://www.5lb.ru/cgi-bin/mp/page.pl?m=docs&jsd=more_items&args="+producer_id+"&offset=1&portion=all_remained")

        products = html.fromstring(first_element + g.response.json['list'])
        for product in products:
            name = product.findtext('.//div[@class="descr"]/div[1]/strong')
            brand = product.findtext('.//div[@class="descr"]/div[2]/span[@class="value"]')
            options = product.xpath('.//div[@class="descr"]/div[3]/span[@class="value"]/select/option')
            sko_select = product.xpath('.//input[@name="id"]')
            exist= product.find('.//div[@class="descr"]/div[@class="line wai clearfix"]')
            if exist is not None:
                continue
            valid_card = product.xpath('.//div[@class="descr"]/div[4]')
            if not valid_card:
                continue
            if options is None or len(options) == 0:
                price = product.findtext('.//div[@class="descr"]/div[3]/span[@class="value"]/b')
            else:
                price = product.findtext('.//div[@class="descr"]/div[4]/span[@class="value"]/b')
            if len(sko_select) > 0:
                sko = sko_select[0].get('value')

            if len(options) > 0:
                for option in options:
                    style = option.get('class') # ext is default value if style is none
                    if style is None:
                        value_selector = option.xpath('.//@value')
                        if len(value_selector) > 0:
                            sko = value_selector[0]
                        else:
                            pass
                        try:
                            crab_product = FiveLB_Product.objects.get(sko=int(sko))
                            if crab_product.brand is None and brand is not None:
                                crab_product.brand = brand
                                crab_product.save()
                        except FiveLB_Product.DoesNotExist:
                            crab_product = FiveLB_Product(name = name, sko=int(sko), brand = brand, price = int(price))
                            crab_product.save()
            else:
                try:
                    crab_product = FiveLB_Product.objects.get(sko=int(sko))
                    if crab_product.brand is None and brand is not None:
                        crab_product.brand = brand
                        crab_product.save()
                except FiveLB_Product.DoesNotExist:
                    crab_product = FiveLB_Product(name = name, sko=int(sko), brand = brand, price = int(price))
                    crab_product.save()

    def task_generator(self):
        yield Task(name='bsn', url=self.target_url)

    def task_bsn(self, grab, task):
        self.parse_products(grab)

#        nav = grab.doc.select('//div[@class="pl_pages clearfix"]')[0]
#        pages = nav.select('.//div[@class="pages"]/a')
#        for page in pages:
#            yield  Task('bsnproducts', url=grab.make_url_absolute(page.attr('href')))

    def task_bsnproducts(self,grab,task):
        self.parse_products(grab)
