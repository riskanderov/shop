# -*- coding: utf-8 -*-
from django import forms

class AppreciationForm(forms.Form):
    author_name = forms.CharField(max_length=30, label=u'Ваше имя')
    rating = forms.IntegerField(max_value=5, min_value=0, label=u'Ваша оценка')
    comment = forms.CharField(max_length=400, label=u'Комментарий')
    product_id = forms.IntegerField()