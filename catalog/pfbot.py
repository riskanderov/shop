# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from models import *


class PFBot:
    def __init__(self, target_url='bsn.html'):
        self.target_url = 'http://www.5lb.ru/producers/'+target_url


    #initial_urls = ['http://www.5lb.ru/producers/bsn.html']
    def run(self):
        driver = webdriver.PhantomJS('phantomjs')
        driver.get(self.target_url)
        driver.find_element_by_xpath('//button[@data-portion="all_remained"]').click()

        try:
            element = WebDriverWait(driver, 10).until_not(lambda driver : driver.find_element_by_css_selector(".get_more"))
        finally:
            cards = driver.find_elements_by_css_selector('#product-list ul li.pa')
            for card in cards:
                name = card.find_element_by_xpath('.//form/div[@class="descr"]/div[1]/strong').text.encode('utf-8')
                brand = card.find_element_by_xpath('.//form/div[@class="descr"]/div[2]/span[@class="value"]').text.encode('utf-8')
                options = card.find_elements_by_xpath('.//form/div[@class="descr"]/div[3]/span[@class="value"]/select/option')
                price = card.find_element_by_css_selector('div.price span.value b').text.encode('utf-8')

                try:
                    sko_input = card.find_elements_by_css_selector('input[name="id"]')
                    if len(sko_input) > 0:
                        sko = sko_input[0].get_attribute('value').encode('utf-8')
                    else:
                        sko = card.get_attribute('data-id').encode('utf-8')
                except:
                    sko = card.get_attribute('data-id').encode('utf-8')
                    pass


                if len(options) > 0:
                    for option in options:
                        style = option.get_attribute('class')  # ext is default value if style is none
                        if style != 'a0':
                            sko = option.get_attribute('value').encode('utf-8')
                            #value_name = option.text.encode('utf-8')

                            try:
                                crab_product = FiveLB_Product.objects.get(sko=int(sko))
                                if crab_product.brand is None and brand is not None:
                                    crab_product.brand = brand
                                    crab_product.save()
                            except FiveLB_Product.DoesNotExist:
                                crab_product = FiveLB_Product(name = name, sko=int(sko), brand = brand, price = int(price))
                                crab_product.save()
                else:
                    try:
                        crab_product = FiveLB_Product.objects.get(sko=int(sko))
                        if crab_product.brand is None and brand is not None:
                            crab_product.brand = brand
                            crab_product.save()
                    except FiveLB_Product.DoesNotExist:
                        crab_product = FiveLB_Product(name = name, sko=int(sko), brand = brand, price = int(price))
                        crab_product.save()

            driver.close()
            driver.quit()