# -*- coding: utf-8 -*-
from models import *
from django.contrib import admin
from moyskald.process import *
from five_lb_spider import *
from pfbot import *
import logging

class OptionValueAdmin(admin.TabularInline):
    model = ProductOption
    extra = 0

def sync_category(modeladmin, request, queryset):
    SyncCategories()
sync_category.short_description = u'Загрузить категории в Мой Склад'

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent', 'is_active', 'available_in_menu', 'menu_order')
    list_editable = ['is_active', 'available_in_menu', 'menu_order']
    actions = [sync_category]

def sync_stock(modeladmin, request, queryset):
    UpdateStock()
sync_stock.short_description = u'Синхронизировать остатки с Моим Складом'

def sync_inventory(modeladmin, request, queryset):
    SyncProducts()
sync_inventory.short_description = u'Загрузить новые товары в Мой Склад'

def sync_fivelb_products(modeladmin, request, queryset):
    logging.basicConfig(level=logging.DEBUG)
    FiveLB_Product.objects.all().delete()
    Product.objects.all().update(fivelb_available=False)
    brands_to_sync = Brand.objects.filter(fivelb_sync_available=True, fivelb_search_page__isnull=False)
    for brand in brands_to_sync:
        bot = PFBot(brand.fivelb_search_page)
        bot.run()

    syncproducts = Product.objects.filter(fivelb_sko__isnull=False, fivelb_sync_active=True)
    for product in syncproducts:
        try:
            fproduct = FiveLB_Product.objects.get(sko=product.fivelb_sko)
            if product.showcase is not None:
                card = ProductCard.objects.get(id=product.showcase.id)
                card.base_price = fproduct.price
                card.save()
            product.base_price = fproduct.price
            product.fivelb_available = True
            product.save()
        except FiveLB_Product.DoesNotExist:
            pass





sync_fivelb_products.short_description = u'Синхронизировать с 5LB'

class ProductCardAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'brand', 'category', 'image','uom', 'uom_units', 'base_price', 'promo', 'description', 'consist', 'take_recommend']}),
        ('Advances options', {'fields': ['is_active', 'is_hit', 'is_recomended','related_products'],
                              'classes': ['collapse']}),
    ]
    list_display = ('name', 'brand', 'uom_units', 'uom', 'base_price', 'promo', 'is_hit', 'is_recomended', 'is_active')
    list_filter = ['brand', 'category']
    list_editable = ['base_price', 'promo', 'is_hit', 'is_recomended', 'is_active']
    filter_horizontal = ('related_products','category',)
    actions = [sync_fivelb_products]

    class Media:
        js = (
            '/static/js/tiny_mce/tiny_mce.js',
            '/static/js/textareas.js',
        )

class ProductAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'brand', 'category', 'image','uom', 'uom_units', 'base_price', 'description', 'consist', 'take_recommend', 'showcase', 'fivelb_sko', 'fivelb_sync_active', 'fivelb_available']}),
        ('Advances options', {'fields': ['is_active', 'is_hit', 'is_recomended','related_products', 'tag'],
                              'classes': ['collapse']}),
    ]
    list_display = ('name', 'brand', 'taste', 'uom_units', 'uom', 'base_price', 'qty_avail', 'is_hit', 'is_recomended', 'is_active', 'fivelb_sko', 'fivelb_sync_active', 'fivelb_available')
    list_filter = ['brand', 'category']
    list_editable = ['base_price', 'qty_avail', 'is_hit', 'is_recomended', 'is_active', 'fivelb_sko', 'fivelb_sync_active', 'fivelb_available']
    inlines = [
    OptionValueAdmin,
    ]
    filter_horizontal = ('related_products','category',)
    actions = [sync_stock, sync_inventory]

    class Media:
        js = (
            '/static/js/tiny_mce/tiny_mce.js',
            '/static/js/textareas.js',
        )
class ClientAdmin(admin.ModelAdmin):
    list_display = ('phone', 'address')

class AppreciationAdmin(admin.ModelAdmin):
    list_display = ('author_name', 'comment', 'date', 'active')
    list_filter = ['date']
    list_editable = ['active']

class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'fivelb_search_page', 'fivelb_sync_available')
    list_filter = ['name']
    list_editable = ['fivelb_search_page', 'fivelb_sync_available']

admin.site.register(ProductCard, ProductCardAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Promo)
admin.site.register(CustomerGroup)
admin.site.register(Client, ClientAdmin)
admin.site.register(Option)
admin.site.register(OptionValue)
admin.site.register(Mission)
admin.site.register(Article)
admin.site.register(ProductComplex)
admin.site.register(Appreciation, AppreciationAdmin)



  