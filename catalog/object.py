class Item(object):
    def __init__(self, itemid, product):
        self.itemid = itemid
        self.product = product

class SeenScope(object):

    def __init__(self):
        self.items = list()
        self.unique_item_id = 0
        self.full_qty = 0

    def _get_next_item_id(self):
        self.unique_item_id += 1
        return self.unique_item_id

    next_item_id = property(_get_next_item_id)

    def add_item(self, product):
        item = Item(self.next_item_id, product)
        contains_this_product = False
        for seen_item in self.items:
            if seen_item.product.id == product.id:
                contains_this_product = True
                break
        if not contains_this_product:
            if len(self.items) == 4:
                self.items.pop(0)
            self.items.append(item)

    @property
    def itemsCount(self):
        try:
            return len(self.items)
        except AttributeError:
            return 0


    def __iter__(self):
        return self.forward()

    def forward(self):
        current_index = 0
        while current_index < len(self.items):
            item = self.items[current_index]
            current_index += 1
            yield item
