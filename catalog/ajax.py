# -*- coding: utf-8 -*-
from dajax.core.Dajax import Dajax
from dajaxice.decorators import dajaxice_register
from dajaxice.utils import deserialize_form
from form import AppreciationForm
from catalog.models import Appreciation, Product
import string
from django.template.loader import render_to_string

@dajaxice_register
def filter_dynamic(request, category_id, brand_id):
    dajax = Dajax()
    return dajax.json()

@dajaxice_register
def send_form(request, form):
    dajax = Dajax()
    form = AppreciationForm(deserialize_form(form))

    if form.is_valid():
        dajax.script('$("#comment_form .input_error").removeClass("input_error")')
        dajax.script('$("#comment_form").hide()')
        dajax.script('$("#no_comments").hide()')
        dajax.script('$("#thanks_for_comment").show()')
        product_id = form.cleaned_data.get('product_id')
        rating = form.cleaned_data.get('rating')
        author_name = form.cleaned_data.get('author_name')
        comment = form.cleaned_data.get('comment')
        try:
            product = Product.objects.get(pk = product_id)
        except Product.DoesNotExist:
            return dajax.json()
        appreciation = Appreciation(
            rating = rating,
            comment = comment,
            author_name = author_name,
            product = product
        )
        appreciation.save()
    else:
        dajax.script('$("#comment_form .input_error").removeClass("input_error")')
        for error in form.errors:
            dajax.script('$("#id_%s").addClass("input_error")' % error)

    return dajax.json()