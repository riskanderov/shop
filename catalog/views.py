# -*- coding: utf-8 -*-
import urllib
from django.contrib.auth.decorators import permission_required
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.db.models import Count, Max, Q
from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import Context, RequestContext
from django.template.loader import get_template
from catalog.models import Product, Category, Mission, ProductComplex, Brand, Appreciation, Article, ProductCard
from tagging.models import Tag, TaggedItem
from catalog.object import SeenScope
import cStringIO as StringIO
from cgi import escape
import ho.pisa as pisa

def index(request):
    recomended_products = ProductCard.objects.annotate(max_qty=Max('products__qty_avail')).filter(max_qty__gt=0, is_recomended=True).order_by('-modified')[:3]
    bestsellers = ProductCard.objects.annotate(max_qty=Max('products__qty_avail')).filter(max_qty__gt=0, is_hit=True).order_by('-modified')[:3]

    proteins = get_cards_from_category('protein')[:3]

    gainers = get_cards_from_category('gainers')[:3]
    bcaa = get_cards_from_category('bcaa')[:3]
    aminos = get_cards_from_category('amino')[:3]
    creatines = get_cards_from_category('creatine')[:3]
    groups = Category.objects.filter(is_active=True, available_in_menu = False)
    articles = Article.objects.all().order_by('-id')[:3]

    c = RequestContext(request, {'recomended':recomended_products,
                                 'bestsellers':bestsellers,
                                 'proteins': proteins,
                                 'gainers': gainers,
                                 'bcaa': bcaa,
                                 'amino': aminos,
                                 'creatines': creatines,
                                 'groups': groups,
                                 'articles':articles,
                                 })
    return render_to_response('catalog/main.html',c)

def get_cards_from_category(categoy):
    category_products = ProductCard.objects.filter(category__slug = categoy, brand__is_active=True, is_active=True).order_by('name')
    product_cards = []
    for card in category_products:
        products_by_card = Product.objects.filter(showcase__id=card.id).filter(Q(qty_avail__gt=0) | Q(fivelb_available=True))
        if product_cards is not None and products_by_card.count() > 0:
            product_cards.append(card)
    return product_cards

def get_cards_brand_filter(category, brand):
    if category != '' and category != 'none':
        if brand != '' and brand != 'none':
            category_products = ProductCard.objects.filter(category__slug = category, brand__slug = brand, brand__is_active=True, is_active=True).order_by('base_price')
        else:
            category_products = ProductCard.objects.filter(category__slug = category, brand__is_active=True, is_active=True).order_by('base_price')
    elif brand != '' and brand != 'none':
        category_products = ProductCard.objects.filter(brand__slug = brand, brand__is_active=True, is_active=True).order_by('base_price')
    elif brand == '':
        category_products = ProductCard.objects.filter(brand__is_active=True, is_active=True).order_by('base_price')
    else:
        return Http404

    product_cards = []
    for card in category_products:
        products_by_card = Product.objects.filter(showcase__id=card.id).filter(Q(qty_avail__gt=0) | Q(fivelb_available=True))
        if product_cards is not None and products_by_card.count() > 0:
            product_cards.append(card)
    return product_cards

def get_cards_category_filter(category, brand):
    if brand != '' and brand != 'none':
        category_products = ProductCard.objects.filter(category__slug = category, brand__is_active=True, brand__slug = brand, is_active=True).order_by('base_price')
    elif category != '' and category != 'none':
        category_products = ProductCard.objects.filter(category__slug = category, brand__is_active=True, is_active=True).order_by('base_price')
    elif category == '':
        category_products = ProductCard.objects.filter(brand__is_active=True, is_active=True).order_by('base_price')
    else:
        raise Http404
    productsResult = Product.objects.filter(Q(qty_avail__gt=0) | Q(fivelb_available=True)).select_related().values('showcase__id').annotate(pcount=Count('showcase'))
    product = {}
    for p in productsResult:
        product[p['showcase__id']] = p['pcount']
    product_cards = []
    for card in category_products:
       # products_by_card = product[card.id]#Product.objects.filter(showcase__id=card.id).select_related().filter(Q(qty_avail__gt=0) | Q(fivelb_available=True))
       if card.id in product:
        if product_cards is not None and product[card.id] > 0:
            product_cards.append(card)
    return product_cards

def brand_filter(request, brand='', category=''):
    title = u'Спортивное питание - интернет-магазин спортивного питания pitfit.ru'
    cat_param = ''
    try:
        cat_param = request.GET.get('cat', '')
        if cat_param != '':
            category = cat_param
    except ValueError:
        pass
    try:
        product_list = get_cards_brand_filter(category, brand)

        if brand != '' and brand != 'none' and category != '' and category != 'none':
            brnd = Brand.objects.get(slug=brand)
            try:
                cat = Category.objects.get(slug=category.lower())
            except Category.DoesNotExist:
                raise Http404
            title = brnd.name +u' - '+ cat.name
        elif brand != '' and brand != 'none' and (category == '' or category == 'none'):
            brnd = Brand.objects.get(slug=brand)
            title = brnd.name
        elif brand == '' or brand == 'none':
            brands = Brand.objects.filter(is_active=True).order_by('image', 'name')
            c = RequestContext(request, {'brands':brands,'title': title})
            return render_to_response('catalog/brands.html', c)
    except (Product.DoesNotExist, Brand.DoesNotExist, Category.DoesNotExist):
        raise Http404

    paginator = Paginator(product_list, 200)
    
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        products = paginator.page(page)
    except (EmptyPage, InvalidPage):
        products = paginator.page(paginator.num_pages)

    if cat_param != '':
        category = ''

    c = RequestContext(request,{'products':products,
                                'filter': 'brand',
                                'brand':brand,
                                'brnd': brnd,
                                'category':category,
                                'title': title})
    return render_to_response('catalog/filtered_products.html',c)

def category_filter(request, category='', brand=''):
    title = u'Спортивное питание - интернет-магазин спортивного питания pitfit.ru'
    brand_param = ''
    try:
        brand_param = request.GET.get('brand', '')
        if brand_param != '':
            brand = brand_param
    except ValueError:
        pass

    try:
        product_list = get_cards_category_filter(category, brand)

        if brand != '' and brand != 'none' and category != '' and category != 'none':
            brnd = Brand.objects.get(slug=brand)
            cat = Category.objects.get(slug=category.lower())
            title = cat.name +u' - '+ brnd.name
        elif (brand == '' or brand == 'none') and category != '' and category != 'none':
            cat = Category.objects.get(slug=category.lower())
            title = cat.name
    except (Product.DoesNotExist, Brand.DoesNotExist, Category.DoesNotExist):
        raise Http404

    paginator = Paginator(product_list, 200)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        products = paginator.page(page)
    except (EmptyPage, InvalidPage):
        products = paginator.page(paginator.num_pages)

    if brand_param != '':
        brand = ''
    c = RequestContext(request,{'products':products,
                                'filter': 'category',
                                'brand':brand,
                                'category':category,
                                'cat': cat,
                                'brnd': brand_param,
                                'title': title})
    return render_to_response('catalog/filtered_products.html',c)

def product_main(request, product_id):
    try:
        product = ProductCard.objects.get(pk = product_id)
        if product is None:
            raise Http404
    except Product.DoesNotExist:
        raise Http404
    appreciations = Appreciation.objects.filter(product = product.id, active=True)
    seenScope = request.session.get('seen_scope', None) or SeenScope()
    seenScope.add_item(product)
    request.session['seen_scope'] = seenScope
    c = RequestContext(request, {'product': product, 'appreciations': appreciations})
    return render_to_response('catalog/product.html',c)

def show_objects_by_tag(request, tag):
    tag = urllib.unquote(unicode(tag))
    tag = get_object_or_404(Tag, name = tag)

    tagged_products = TaggedItem.objects.get_by_model(Product, tag)

    c = RequestContext(request, {'products': tagged_products, 'title': tag.name})
    return render_to_response('catalog/tagged_products.html', c)

def yml(request):
    products = Product.objects.filter(is_active=True).filter(Q(qty_avail__gt=0) | Q(fivelb_available=True))
    categories = Category.objects.filter(is_active=1)
    c = RequestContext(request, {'categories': categories, 'products':products})
    return render_to_response('catalog/yml.html', c, mimetype="application/xml")

def moysklad_yml(request):
    products = Product.objects.all()
    categories = Category.objects.filter(is_active=1)
    c = RequestContext(request, {'categories': categories, 'products':products})
    return render_to_response('catalog/moyskald_yml.html', c,  mimetype="application/xml")

def moysklad_receipt_csv(request):
    products = Product.objects.filter(qty_avail__gt=0, base_price__gt=0)
    c = RequestContext(request, {'products':products})
    return render_to_response('catalog/moyskald_receipt_csv.html', c)

@permission_required('catalog.delete_product')
def products_out_of_stock(request):
    products = Product.objects.filter(qty_avail=0).order_by('brand', 'name')
    c = RequestContext(request, {'products': products})
    return render_to_response('catalog/product_out_of_stock.html', c)

def show_mission_complexes(request, mission):
    mission = Mission.objects.get(slug=mission)
    complexes = mission.complex.all()
    c = RequestContext(request, {'complexes':complexes, 'mission': mission})
    return render_to_response('catalog/mission_complexes.html', c)


def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, encoding='UTF-8')
    if not pdf.err:
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))

@permission_required('catalog.delete_product')
def full_product_list(request):
    products = Product.objects.all().order_by('brand', 'name')
    c = RequestContext(request, {'products': products })
    return render_to_response('catalog/full_product_list.html', c)

@permission_required('catalog.delete_product')
def catalog_price(request, discount='15'):
    catalog=[]
    items = Product.objects.filter(qty_avail__gt=0, base_price__gt=0).order_by('brand', 'name')
    if int(discount) in [5, 7, 10, 15, 20, 25]:
        for item in items:
            item.base_price = int(item.base_price * (1 - int(discount)/100))
            catalog.append(item)
    elif int(discount) == 0:
        for item in items:
            catalog.append(item)
    return render_to_pdf('catalog/price_list.html', {'pagesize': 'A4', 'products': catalog, 'discount': int(discount)})