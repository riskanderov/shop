# -*- coding: utf-8 -*-
from django import forms
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.encoding import smart_unicode

def validate_new_customer_email(value):
    try:
        user = User.objects.get(email=value)
        return ValidationError(u'Email занят')
    except User.DoesNotExist:
        pass
    

class RegisterForm(forms.Form):
    email = forms.EmailField(label=u'E-mail')
    name = forms.CharField(label=u'Имя')
    password1 = forms.CharField(label=u'Пароль')
    password2 = forms.CharField(label=u'Пароль еще раз')

    def clean(self):
        cleaned_data = self.cleaned_data
        email = cleaned_data.get("email")
        name = cleaned_data.get("name")
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")
        try:
            user = User.objects.get(email=email)
            msg = u'Email занят'
            self._errors['email'] = self.error_class([msg])
            del cleaned_data['email']
        except User.DoesNotExist:
            pass

        if password1 != password2:
            msg = u'Пароль не совпадает'
            self._errors['password2'] = self.error_class([msg])
            if cleaned_data.get('password2', None):
                del cleaned_data['password2']
            if cleaned_data.get('password1', None):
                del cleaned_data['password1']

        return cleaned_data
        

class LoginForm(forms.Form):
    email = forms.EmailField(label=u'E-mail')
    password = forms.CharField(label=u'Пароль')

    def clean(self):
        cleaned_data = self.cleaned_data
        email = cleaned_data.get('email', None)
        password = cleaned_data.get('password', None)

        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            msg = u'Неправильный Email'
            self._errors['email'] = self.error_class([msg])
            return cleaned_data


        user = auth.authenticate(email=email, password=password)

        if user is None:
            self._errors['password'] = self.error_class([u'Неверный пароль'])

        return cleaned_data



