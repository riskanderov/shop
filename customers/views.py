import user
from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from catalog.models import CustomerGroup
from customers.forms import RegisterForm, LoginForm
from order.forms import OrderForm

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = auth.authenticate(email=email, password=password)

            if user is not None and user.is_active:
                cart = request.session.get('cart', None)
                auth.login(request, user)

                if cart is None:
                    return HttpResponseRedirect('/')
                else:
                    form = OrderForm()
                    c = RequestContext(request, {'cart': cart,
                                                 'form': form,
                                                 'customer': request.user,
                                                 'step': 'checkout'})

                    return render_to_response('cart/checkout.html', c)
            else:
                #TODO Bad login error page or something else
                return HttpResponseRedirect('/brand/bsn')
    else:
        form = LoginForm()
    c = RequestContext(request, {'login_form': form})
    return render_to_response('customers/login.html', c)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            name = form.cleaned_data['name']
            password1 = form.cleaned_data['password1']
            password2 = form.cleaned_data['password2']
            try:
                user = User.objects.get(email = email)
            except User.DoesNotExist:
                if password1 != password2:
                    return HttpResponseRedirect('/brand/bsn')
                new_user = User.objects.create_user(email, email, password1)
                new_user.first_name = name
                new_user.save()
                user = auth.authenticate(email=email, password=password1)
                cart = request.session.get('cart', None)
                auth.login(request, user)
                if cart is None:
                    return HttpResponseRedirect('/')
                else:
                    form = OrderForm()
                    c = RequestContext(request, {'cart': cart,
                                                 'form': form,
                                                 'customer': request.user,
                                                 'step': 'checkout'})

                    return render_to_response('cart/checkout.html', c)
    else:
        form = RegisterForm()
    c = RequestContext(request, {'reg_form': form})
    return render_to_response('customers/register.html',c)