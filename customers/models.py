# -*- coding: utf-8 -*-
import datetime
from django.contrib.auth.models import User, UserManager
from django.db import models
from django.db.models.signals import post_save
from catalog.models import CustomerGroup

class CustomerProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    phone = models.CharField(max_length=20, default='none')
    newsletter = models.BooleanField(default=False)
    discount = models.IntegerField(default=0)

    def __unicode__(self):
        return '%s (%s)' % (self.user.first_name, self.user.username)
    class Meta:
        verbose_name_plural = u'Клиенты'

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        CustomerProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)

class CustomerAddress(models.Model):
    customer = models.ForeignKey(User)
    address = models.TextField(blank=True)
    submit_date = models.DateTimeField(blank=True)