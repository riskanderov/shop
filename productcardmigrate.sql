
update catalog_product p set showcase_id = s.id from catalog_productcard s where s.name = p.name and s.uom_units = p.uom_units

insert into catalog_productcard_category(productcard_id, category_id)
select distinct p.showcase_id, cpc.category_id
from  catalog_product p
inner join catalog_product_category cpc on cpc.product_id = p."id"
where p.showcase_id is not null
order by p.showcase_id
