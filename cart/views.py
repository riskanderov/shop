# -*- coding: utf-8 -*-
from django.contrib import auth
from django.contrib.auth.models import User
from django.http import *
from django.shortcuts import render_to_response
from django.template.context import  RequestContext
from cart.object import Cart
from catalog.models import  Client
from customers.forms import LoginForm
from order.forms import OrderForm, FastOrderForm, RegOrderForm
from order.models import Order, ShipmentType

def cart(request):
    cart = request.session.get('cart', None) or Cart()
    request.session['cart']=cart
    full_price = cart.cart_full_price
    discount = get_discount(full_price)
    discount_amount = int(round(cart.cart_full_price_for_discount*(float(discount)/100)))

    next_discount = 5
    next_discount_step = 10000 - cart.cart_full_price
    if discount == 5:
        next_discount = 10
        next_discount_step = 15000 - cart.cart_full_price
    elif discount == 10:
        next_discount = 10
        next_discount_step = 0
    elif discount == 15:
        next_discount = 20
        next_discount_step = 100000 - cart.cart_full_price
    elif discount == 20:
        next_discount = 20
        next_discount_step = 0


    additional_products = list()
    if cart.cart_full_qty > 0:
        for item in cart:
            additional_products.extend(item.product.related_products.filter(qty_avail__gt=0).all())


    c = RequestContext(request,{'cart':cart,
                                'cart_price': full_price,
                                'discount_amount': discount_amount,
                                'discount': discount,
                                'next_discount': next_discount,
                                'next_discount_step': next_discount_step,
                                'full_order_amount': full_price - discount_amount,
                                'related_products': additional_products[:6],
                                'step': 'cart'})
    
    return render_to_response('cart/main.html',c)

def check_out(request):
    cart = request.session.get('cart', None)
    if cart is None or cart.cart_full_qty < 1:
        return render_to_response('cart/empty_cart.html', RequestContext(request,{}))

    discount = get_discount(cart.cart_full_price)
    discount_amount = int(round(cart.cart_full_price_for_discount*(float(discount)/100)))

    full_order_amount = cart.cart_full_price - discount_amount
    default_shipment_type = ShipmentType.objects.filter(default = True)[0]
    if cart.cart_full_price < 6000:
        full_order_amount = cart.cart_full_price - discount_amount + default_shipment_type.price

    shipment_types = ShipmentType.objects.all().order_by('id')
    if request.user.is_authenticated():
        form = OrderForm()
        c = RequestContext(request, {'cart': cart,
                                     'form': form,
                                     'customer': request.user,
                                     'discount': discount,
                                     'discount_amount': discount_amount,
                                     'full_order_amount': full_order_amount,
                                     'step': 'checkout',
                                     'shipment_types': shipment_types})

        return render_to_response('cart/checkout.html', c)

    login_form = LoginForm()
    form = FastOrderForm()
    c = RequestContext(request,{'form': form,
                                'cart': cart,
                                'login_form': login_form,
                                'discount': discount,
                                'discount_amount': discount_amount,
                                'full_order_amount': full_order_amount,
                                'step': 'checkout',
                                'shipment_types': shipment_types})
    
    return render_to_response('cart/auth.html', c)

def create_order(request):
    cart = request.session.get('cart', None)
    if cart is None or cart.cart_full_qty < 1:
        return render_to_response('cart/empty_cart.html', RequestContext(request))
    shipment_types = ShipmentType.objects.all().order_by('id')

    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            user = User.objects.get(email=request.user.email)
            shipment_type = ShipmentType.objects.get(type_id = form.cleaned_data.get('shipment_type'))
            shipment_price = shipment_type.price
            if shipment_type.type_id == 'msk' and cart.cart_full_price >= 6000:
                shipment_price = 0
            order = Order(customer = user,
                          contact_phone=form.cleaned_data.get('phone'),
                          shipment_address=form.cleaned_data.get('address'),
                          order_comment=form.cleaned_data.get('comment'),
                          shipment_type = shipment_type,
                          shipment_price = shipment_price)

            order.save()
            full_price = create_order_details(order, cart)
            
            discount = get_discount(full_price)
            discount_amount = int(round(cart.cart_full_price_for_discount*(float(discount)/100)))
            request.session['cart'] = Cart()

            full_order_amount = cart.cart_full_price - discount_amount + shipment_price

            order.order_price = full_order_amount
            order.save()

            c = RequestContext(request,{'order': order,
                                        'full_price': full_price,
                                        'discount_amount': discount_amount,
                                        'discount': discount,
                                        'discounted_order': full_order_amount,
                                        'step': 'order_saved'})
            
            return render_to_response('cart/checkout_done.html', c)
    else:
        form = OrderForm()
    c = RequestContext(request,{'cart':cart,
                                'form': form,
                                'step': 'checkout',
                                'shipment_types': shipment_types})
    
    return render_to_response('cart/checkout.html',c)

def create_reg_order(request):
    cart = request.session.get('cart', None)
    if request.method != 'POST':
        raise Http404
    if cart is None or cart.cart_full_qty < 1:
        return render_to_response('cart/empty_cart.html', RequestContext(request))

    form = RegOrderForm(request.POST)
    shipment_types = ShipmentType.objects.all().order_by('id')

    if form.is_valid():
        email = form.cleaned_data['email']
        name = form.cleaned_data['name']
        password1 = form.cleaned_data['password1']
        password2 = form.cleaned_data['password2']
        try:
            User.objects.get(email = email)
        except User.DoesNotExist:
            if password1 != password2:
                raise Http404
            new_user = User.objects.create_user(email, email, password1)
            new_user.first_name = name
            new_user.save()
            user = auth.authenticate(email=email, password=password1)
            auth.login(request, user)
            shipment_type = ShipmentType.objects.get(type_id = form.cleaned_data.get('shipment_type'))
            shipment_price = shipment_type.price
            if shipment_type.type_id == 'msk' and cart.cart_full_price >= 6000:
                shipment_price = 0
            order = Order(customer = user,
                          contact_phone=form.cleaned_data.get('phone'),
                          shipment_address=form.cleaned_data.get('address'),
                          order_comment=form.cleaned_data.get('comment'),
                          shipment_type = shipment_type,
                          shipment_price = shipment_price)

            order.save()
            full_price = create_order_details(order, cart)
            
            discount = get_discount(full_price)
            discount_amount = int(round(cart.cart_full_price_for_discount*(float(discount)/100)))

            full_order_amount = cart.cart_full_price - discount_amount + shipment_price
            order.order_price = full_order_amount
            order.save()

            request.session['cart'] = Cart()
            c = RequestContext(request,{'order': order,
                                        'full_price': full_price,
                                        'discount_amount': discount_amount,
                                        'discount': discount,
                                        'discounted_order': full_order_amount,
                                        'step': 'order_saved'})
            
            return render_to_response('cart/checkout_done.html', c)

    login_form = LoginForm()
    discount = get_discount(cart.cart_full_price)
    discount_amount = int(round(cart.cart_full_price_for_discount*(float(discount)/100)))

    full_order_amount = cart.cart_full_price - discount_amount
    if cart.cart_full_price < 6000:
        full_order_amount = cart.cart_full_price - discount_amount + 160

    c = RequestContext(request,{'form': form,
                                'login_form': login_form,
                                'step': 'checkout',
                                'type': 'reg',
                                'cart': cart,
                                'discount': discount,
                                'discount_amount': discount_amount,
                                'full_order_amount': full_order_amount,
                                'shipment_types': shipment_types})
    
    return render_to_response('cart/auth.html',c)

def create_fast_order(request):
    cart = request.session.get('cart', None)
    if cart is None or cart.cart_full_qty < 1:
        return render_to_response('cart/empty_cart.html', RequestContext(request))
    shipment_types = ShipmentType.objects.all().order_by('id')
    default_shipment_type = ShipmentType.objects.filter(default = True)[0]
    if request.method == 'POST':
        form = FastOrderForm(request.POST)
        if form.is_valid():
            client = Client(phone=form.cleaned_data.get('phone'),
                            address=form.cleaned_data.get('address'),
                            name=form.cleaned_data.get('name'),
                            email = form.cleaned_data.get('email'))
            client.save()

            shipment_type = ShipmentType.objects.get(type_id = form.cleaned_data.get('shipment_type'))
            shipment_price = shipment_type.price
            if shipment_type.type_id == 'msk' and cart.cart_full_price >= 6000:
                shipment_price = 0
            order = Order(anonym=client,
                          contact_phone=client.phone,
                          shipment_address=client.address,
                          shipment_type = shipment_type,
                          shipment_price = shipment_price)
            order.save()
            full_price = create_order_details(order, cart)

            discount = get_discount(cart.cart_full_price)
            discount_amount = int(round(cart.cart_full_price_for_discount*(float(discount)/100)))
            full_order_amount = cart.cart_full_price - discount_amount + shipment_price
            order.order_price = full_order_amount
            order.save()

            request.session['cart'] = Cart()
            c = RequestContext(request,{'order': order,
                                        'full_price': full_price,
                                        'discount_amount': discount_amount,
                                        'discount': discount,
                                        'discounted_order': full_order_amount,
                                        'step': 'order_saved'})
            
            return render_to_response('cart/checkout_done.html', c)
    else:
        form = FastOrderForm()
    login_form = LoginForm()
    discount = get_discount(cart.cart_full_price)
    discount_amount = int(round(cart.cart_full_price_for_discount*(float(discount)/100)))

    full_order_amount = cart.cart_full_price - discount_amount
    if cart.cart_full_price < 6000:
        full_order_amount = cart.cart_full_price - discount_amount + default_shipment_type.price

    c = RequestContext(request,{'form': form,
                                'login_form': login_form,
                                'step': 'checkout',
                                'type': 'fast',
                                'cart': cart,
                                'discount': discount,
                                'discount_amount': discount_amount,
                                'full_order_amount': full_order_amount,
                                'shipment_types': shipment_types})
    
    return render_to_response('cart/auth.html',c)

def get_discount(full_price):

    if full_price >= 15000:
        return 10
    elif full_price >= 10000:
        return 5
    else:
        return 0

def get_shipment_price(full_price):
    if full_price >= 6000:
        return 0
    return 250

def create_order_details(order, cart):
    full_price = 0
    for item in cart:
        if not item.quantity:
            continue
        full_price += item.quantity*item.product_card.customer_price
        order.details.create(
            product = item.product,
            qty = item.quantity,
            price = item.product_card.customer_price
        )
    return full_price
