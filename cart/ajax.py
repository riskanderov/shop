# -*- coding: utf-8 -*-
from dajax.core.Dajax import Dajax
from dajaxice.decorators import dajaxice_register
import string
from django.template.loader import render_to_string
from cart.object import Cart
from catalog.models import Product, ProductCard
from order.models import ShipmentType

@dajaxice_register
def add_to_cart(request, product_id):
    cart = request.session.get('cart', None) or Cart()
    
    product = Product.objects.get(id=product_id)
    product_card = ProductCard.objects.get(id=product.showcase.id)
    update = False
    for item in cart:
        if product.id == item.product.id:
            update = True
            item.quantity += 1

    if not update:
        cart.add_item(product, product_card)
    request.session['cart'] = cart
    render = render_to_string('cart.html', {'cart':cart})
    render_mini_total = render_to_string('cart_mini_total.html', {'cart': cart})
    dajax = Dajax()
    dajax.assign('#minicart', 'innerHTML', render)
    dajax.assign('#cart_mini_total', 'outerHTML', render_mini_total)
    dajax.redirect('http://pitfit.ru/cart', delay=0)
    return dajax.json()

@dajaxice_register
def add_related_to_cart(request, product_id):
    dajax = Dajax()
    cart = request.session.get('cart', None) or Cart()

    product = Product.objects.get(id=product_id)
    update = False
    for item in cart:
        if product.id == item.product.id:
            update = True
            item.quantity += 1

    if not update:
        cart.add_item(product)


    request.session['cart'] = cart

    dajax.script('window.location.href="/cart"')

    return dajax.json()

@dajaxice_register
def delete_from_cart(request, product_id):
    cart = request.session.get('cart', None) or Cart()
    dajax = Dajax()
    remove = False
    product = Product.objects.get(id=product_id)
    for item in cart:
        if item.product.id == product_id:
            dajax.remove('#cart_row%s' % product_id)
    if remove:
        cart.remove_item(product)

    request.session['cart'] = cart
    return dajax.json()

@dajaxice_register
def update_cart_item(request, product_id, kind):
    cart = request.session.get('cart', None) or Cart()
    dajax = Dajax()
    for item in cart:
        if item.product.id == product_id:
            if kind == 'up':
                item.quantity += 1
                item.price = item.product_card.customer_price * item.quantity
            elif kind == 'down':
                item.quantity -= 1
                item.price = item.product_card.customer_price * item.quantity
            if item.quantity <= 0:
                item.quantity = 0
                item.price = item.product_card.customer_price * item.quantity
                dajax.script('$("#cart_row%s").addClass("to_delete")' % product_id)
            if item.quantity > 0:
                dajax.script('$("#cart_row%s").removeClass("to_delete")' % product_id)
            dajax.assign('#line%s_price' % item.product.id, 'innerHTML', item.price)
            dajax.assign('#cart_item_qty%s div' % item.product.id, 'innerHTML', item.quantity)

    cart_full_qty_plur = ''
    if cart.cart_full_qty in (1,21,31,41,51,61,71,81,91,101):
        cart_full_qty_plur = str(cart.cart_full_qty) + u' товар'
    if 1 < cart.cart_full_qty <= 4 or cart.cart_full_qty > 21 and cart.cart_full_qty not in (21,31,41,51,61,71,81,91,101):
        cart_full_qty_plur = str(cart.cart_full_qty) + u' товарa'
    if 4 < cart.cart_full_qty < 21 and cart.cart_full_qty not in (21,31,41,51,61,71,81,91,101):
        cart_full_qty_plur = str(cart.cart_full_qty) + u' товаров'

    dajax.assign('#full_cart_qty', 'innerHTML', cart_full_qty_plur)
    dajax.assign('#full_cart_price', 'innerHTML', cart.cart_full_price)

    discount = get_discount(cart.cart_full_price)
    next_discount = 5
    to_next_discount_amount = 10000 - cart.cart_full_price
    if discount == 5:
        next_discount = 10
        to_next_discount_amount = 15000 - cart.cart_full_price
    elif discount == 10:
        next_discount = 15
        to_next_discount_amount = 0 #50000 - cart.cart_full_price
    elif discount == 15:
        next_discount = 20
        to_next_discount_amount = 100000 - cart.cart_full_price
    elif discount > 15:
        next_discount = 20
        to_next_discount_amount = 0
    if to_next_discount_amount > 0:
        dajax.script('$("#more_discount_msg").show()')
    else:
        dajax.script('$("#more_discount_msg").hide()')

    discount_amount = int(round(cart.cart_full_price_for_discount*(float(discount)/100)))
    dajax.assign('#current_discount', 'innerHTML', discount)
    dajax.assign('#next_discount', 'innerHTML', next_discount)
    dajax.assign('#next_discount_step', 'innerHTML', to_next_discount_amount)
    dajax.assign('#discount_amount', 'innerHTML', discount_amount)
    dajax.assign('#full_order_amount', 'innerHTML', cart.cart_full_price - discount_amount)

    request.session['cart'] = cart
    return dajax.json()

@dajaxice_register
def calculate_shipment_price(request, type):
    cart = request.session.get('cart', None) or Cart()
    dajax = Dajax()

    ship_price = ShipmentType.objects.get(type_id = type).price
    if cart.cart_full_price >= 6000:
        ship_price = 0

    if type == 'msk':
        dajax.script('$("#shipment_msk").attr("checked", "checked");')
    if type == 'office':
        dajax.script('$("#shipment_office").attr("checked", "checked");')
    if type == 'ems':
        dajax.script('$("#shipment_ems").attr("checked", "checked");')

    dajax.assign('#shipment_price', 'innerHTML', ship_price)
    discount = get_discount(cart.cart_full_price)
    order_with_discount = cart.cart_full_price - int(round(cart.cart_full_price_for_discount*(float(discount)/100)))
    dajax.assign('#full_order_amount', 'innerHTML', order_with_discount + ship_price)

    return dajax.json()

def get_discount(full_price):
    if full_price >= 15000:
        return 10
    elif full_price >= 10000:
        return 5
    else:
        return 0

@dajaxice_register
def clear_cart(request):
    request.session['cart'] = Cart()
    render = render_to_string('cart.html', {'cart':Cart()})
    render_mini_total = render_to_string('cart_mini_total.html', {'cart': Cart()})

    dajax = Dajax()
    dajax.assign('#minicart', 'innerHTML', render)
    dajax.assign('#cart_mini_total', 'outerHTML', render_mini_total)
    dajax.script('$("#related_products_block").hide();')
    dajax.script('window.location.href="/cart"')
    return dajax.json()

