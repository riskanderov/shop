from django import template
from cart.object import Cart
from catalog.models import *

register = template.Library()

@register.inclusion_tag('cart.html', takes_context=True)
def show_cart_mini_items(context):
    cart = None
    if 'request' in context:
        request = context['request']
        cart = request.session.get('cart', None) or Cart
        try:
            cart.items
        except AttributeError:
            cart = Cart()

    return {'cart': cart}

@register.inclusion_tag('cart_mini_total.html', takes_context=True)
def show_cart_mini_total(context):
    cart = None
    if 'request' in context:
        request = context['request']
        cart = request.session.get('cart', None) or Cart
        try:
            cart.items
        except AttributeError:
            cart = Cart()
    return {'cart': cart}

@register.inclusion_tag('cart/order_steps.html', takes_context=True)
def show_order_steps(context):
    step = context['step']
    return {'step': step}

@register.inclusion_tag('cart/order_line.html', takes_context=True)
def order_line(context, item):
    return {'item': item}
