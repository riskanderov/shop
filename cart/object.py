class Item(object):
    def __init__(self, itemid, product, product_card, quantity=1):
        self.itemid = itemid
        self.product = product
        self.quantity = quantity
        self.product_card = product_card
        self.price = product_card.customer_price * quantity

class Cart(object):

    def __init__(self):
        self.items = list()
        self.unique_item_id = 0
        self.full_qty = 0
        self.full_price = 0
        self.full_price_for_discount = 0

    def _get_next_item_id(self):
        self.unique_item_id += 1
        return self.unique_item_id
    def _get_full_qty(self):
        self.full_qty = 0
        for item in self.items:
            self.full_qty += item.quantity
        return self.full_qty
    def _get_full_price(self):
        self.full_price = 0
        for item in self.items:
            self.full_price += item.quantity*item.product_card.customer_price
        return self.full_price
    def _get_full_price_for_discount(self):
        self.full_price_for_discount = 0
        for item in self.items:
            if item.product_card.promo is None:
                self.full_price_for_discount += item.quantity*item.product_card.customer_price
        return self.full_price_for_discount

    cart_full_qty = property(_get_full_qty)
    cart_full_price = property(_get_full_price)
    cart_full_price_for_discount = property(_get_full_price_for_discount)
    next_item_id = property(_get_next_item_id)

    def add_item(self, product, product_card, quantity=1):
        item = Item(self.next_item_id, product, product_card, quantity)
        self.items.append(item)
        return  item

    def remove_item(self, itemid):
        self.items = filter(lambda x: x.itemid != itemid, self.items)

    def __iter__(self):
        return self.forward()

    def forward(self):
        current_index = 0
        while current_index < len(self.items):
            item = self.items[current_index]
            current_index += 1
            yield item